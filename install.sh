 
#!/bin/bash
echo "################################";
echo "start UPDATE of symfony project";
echo "################################";
echo " ";
echo "####### fix file permissions ######";

`which setfacl` > TEST
if [ echo $TEST == 'acl not found' ]; then
    echo "acl is not installed"
    sudo apt install acl -y
    exit
else
    echo "acl est bien là"
    acl -v
fi
HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)

sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var

echo "####### phpunit test suite ######";
`which phpunit` > TEST
if [ echo $TEST == 'phpunit not found' ]; then
    echo "phpunit is not installed"
    sudo apt install phpunit -y
    exit
else
    echo "phpunit est bien là"
    phpunit -v
fi

echo "####### node packages ######";
`which npm` > TEST
if [ echo $TEST == 'npm not found' ]; then
    echo "npm is not installed"
    sudo apt install npm -y
    exit
else
    echo "npm est bien là"
    npm -v
fi


`which yarn` > TEST
if [ echo $TEST == 'yarn not found' ]; then
    echo "yarn is not installed"
    sudo npm i -g yarn
    exit
else
    echo "yarn est bien là"
    yarn -v
    
    echo "####### build front end with yarn ######";
    yarn
    yarn run encore production
fi

echo "####### symfony vendor packages ######";

`which composer` > TEST
if [ echo $TEST == 'composer not found' ]; then
    echo "composer is not installed"
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    php composer-setup.php
    php -r "unlink('composer-setup.php');"
    exit
else
    echo "composer est bien là"
    composer -v
    composer self-update
    composer update
fi

echo "####### symfony database ######";
php bin/console doctrine:schema:update --dump-sql
php bin/console doctrine:schema:update --force
php bin/console cache:clear -eprod
php bin/console cache:warmup -eprod

echo "################################";
echo "end UPDATE of symfony project";
echo "################################";

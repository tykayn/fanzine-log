<?php

namespace AppBundle\Traits;

trait Commentable {

	/**
	 * @ORM\Column(type="string", length=256 , nullable=true)
	 */
	private $comment;

	/**
	 * @return mixed
	 */
	public function getComment() {
		return $this->comment;
	}

	/**
	 * @param mixed $comment
	 */
	public function setComment( $comment ) {
		$this->comment = $comment;
	}


}

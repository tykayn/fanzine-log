<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FestivalType extends AbstractType {
	/**
	 * {@inheritdoc}
	 */
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder
			->add( 'name' )
			->add( 'fraisInscription' )
			->add( 'fraisTransport' )
			->add( 'fraisHebergement' )
			->add( 'fraisRepas' )
			->add( 'chiffreAffaire' )
			->add( 'fondDeCaisseAvant' )
			->add( 'fondDeCaisseApres' )
			->add( 'dateCreation' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions( OptionsResolver $resolver ) {
		$resolver->setDefaults( [
			'data_class' => 'AppBundle\Entity\Festival',
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'appbundle_festival';
	}


}
